export const rooms = [
  {
    'id': 236,
    'createdAt': '2022-10-22T01:09:49.600Z',
    'idLiked': 515,
    'idCrusher1': 126,
    'idCrusher2': 109,
    'crusher1': {
      'id': 126,
      'name': 'Bruna',
      'photos': [
        {
          'id': 315,
          'profile': 1
        }
      ]
    },
    'crusher2': {
      'id': 109,
      'name': 'hugo',
      'photos': [
        {
          'id': 333,
          'profile': 1
        }
      ]
    },
    'roomId': 236,
    'roomName': 'Lots of Messages ROOM',
    'users': [
      {
        '_id': 126,
        'username': 'Bruna',
        'status': {
          'lastChanged': 'today, 14:30'
        }
      },
      {
        '_id': 109,
        'username': 'hugo',
        'status': {
          'lastChanged': 'today, 14:30'
        }
      }
    ]
  }
]
